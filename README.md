# This is a small C++ wrapper around libevent to provide message oriented send/receive async API.

The library purpose is to abstract from libevent details and headers and provide simple message-based interface to send/receive structures over the network.

It is used in our VoIP quality analysing distributed solutions to provide connectivity within these solutions.

One can read more about our tools and technologies at https://sevana.biz.

# How to use it ?

Please use CMakeLists.txt file from source/ folder. It is simple as much as possible.
Supported OS is Linux; macOS should be fine too.

# API.

Please remember - API is async.
Instantiate proto::msgserver and proto::msgclient and start() them. Please register neccessary callbacks.
The target address can be IP only for now.

# Roadmap.

- add Windows support
- async DNS resolver
- high performance API

# License

BSD license - please see source files.


Please send us your questions & comments to development@sevana.biz

Thank you!